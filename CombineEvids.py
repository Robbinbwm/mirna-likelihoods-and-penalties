#Author:            Robbin Bouwmeester (supervisors: Johan Westerhuis and Frans van der Kloet)
#Purpose:           Combining the evidence codes of multiple files
#Date:              18-09-2015
#Python version:    2.7.5
#Bugs:              No bugs found...

import sys

"""Function that sums the bits in two vectors, but the max is still "1"
        Inputs:
            s1 - vector 1
			s2 - vector 2
        Outputs:
             - the summed vector
"""
def sum_vectors(s1,s2):
	if len(s1) != len(s2):
		print len(s1)
		print len(s2)
		print [s1]
		print [s2]
		print "Make sure vectors are of equal size!"
		print "Exiting..."
		sys.exit(-1)
	
	#Make a new vector
	new_vec = ["0"]*50
	
	#Iterate over vector and flip bits to "1" if needed
	for c1 in range(0,len(s1)):
		if s1[c1] != "0": new_vec[c1] = "1"
	for c2 in range(0,len(s2)):
		if s2[c2] != "0": new_vec[c2] = "1"
	
	#Return new vector
	return "".join(new_vec)

"""Function that puts the values (bit vectors) of one matrix in the other
        Inputs:
            matrix_orig - matrix that needs to be added to the other matrix
			matrix_fill - matrix where the new values need to go
        Outputs:
            matrix_orig - matrix that is filled
"""	
def fill_matrix(matrix_orig,matrix_fill):
	#Iterate over rows and columns of matrix
	for row in range(0,len(matrix_orig)):
		for column in range(0,len(matrix_orig[0])):
			if matrix_orig[row][column] == "0": matrix_orig[row][column] = "".join(["0"]*50)
			if matrix_fill[row][column] == "0": continue
			#Sum the bit vectors
			matrix_orig[row][column] = sum_vectors(matrix_orig[row][column],matrix_fill[row][column])
	return matrix_orig

"""Function that reads a matrix and puts it into a two dimensional list
	Inputs:
		infile - tab and hard return seperated matrix
	Outputs:
		 - two-dimensional list of matrix
"""	
def read_matrix(infile,verbose=False):
	global nrow
	global ncol
	
	if verbose: print infile
	
	#Make an initial matrix with "0" values
	matrix = [["0" for i in range(nrow)] for j in range(ncol)]
	
	#Read the matrix
	infile = open(infile)
	infile = infile.readlines()

	#Iterate over rows and columns
	for row in range(1,len(matrix)+1):
		row_line = infile[row].strip().split("\t")
		for column in range(1,len(matrix[0])+1):
			try:
				#Set the values in initial matrix
				val = row_line[column].split("-")[0]
				matrix[row-1][column-1] = val
			except:
				print matrix[row]
				print len(matrix[row])
				print column
	return matrix

"""Function that writes a matrix to a file that is tab and hard return seperated
	Inputs:
		matrix - two-dimensional list that needs to be written
	Outputs:
		 - 
"""	
def write_matrix(matrix,outfileName="combined.txt"):
	outfile = open(outfileName,"w")
	for row in matrix:
		outfile.write("\t".join(row))
		outfile.write("\n")
	outfile.close()

"""Function that returns the dimensions of a two-dimensional matrix that is tab and hard return seperated
	Inputs:
		infile - tab and hard return seperated matrix
	Outputs:
		 - the number of columns and the number of rows
"""	
def get_dim_matrix(infile,verbose=False):
	if verbose: print infile
	infile = open(infile)
	infile = infile.readlines()
	
	#Determine dimensions and return
	return(len(infile),len(infile[0].split("\t")))

#Get the number of columns and rows	
ncol,nrow = get_dim_matrix("pictar.txt")

#Adjust for the first column that contains the mRNA identifiers
ncol = ncol-1

#Make an initial matrix
matrix = [["0" for i in range(nrow)] for j in range(ncol)]

#Read adjacency matrix with evidence codes and add it to the combined matrices
reptar = read_matrix("reptar.txt")
matrix = fill_matrix(matrix,reptar)

targetscan = read_matrix("targetscan.txt")
matrix = fill_matrix(matrix,targetscan)

pictar = read_matrix("pictar.txt")
matrix = fill_matrix(matrix,pictar)

mirwalk = read_matrix("mirwalk.txt")
matrix = fill_matrix(matrix,mirwalk)

#Write the composite adjacency matrix to a file
write_matrix(matrix)