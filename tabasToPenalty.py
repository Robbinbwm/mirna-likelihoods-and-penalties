infile = open("tabas.txt")

wsp_dict = {}

for line in infile:
	line = line.strip()
	splitline = line.split(";")
	wsp_score = splitline[4]
	mirna_name = splitline[0]
	gene_name = splitline[2]
	try: 
		wsp_dict[mirna_name]
	except:
		wsp_dict[mirna_name] = {}
	wsp_dict[mirna_name][gene_name] = wsp_score
	
infile_genes = open("Data/genes.txt")
infile_genes = [l.strip() for l in infile_genes]

infile_mirna = open("Data/miRNA.txt")
infile_mirna = [l.split(",")[0] for l in infile_mirna]

outfile = open("tabas_penal.txt","w")
print("hier")
counter = 0
for line in infile_mirna:
	new_l = []
	for g in infile_genes:
		try:
			new_l.append("%s" % (float(wsp_dict[line][g])+1.0))
			counter += 1
		except:
			new_l.append("0.0")
	outfile.write("\t".join(new_l))
	outfile.write("\n")
	outfile.flush()
	
	print("=======")
	print(counter)
outfile.close()