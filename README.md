# README #

# Likelihood of association and penalties #

Here the steps will be described to calculate the likelihood of association and put them into an adjacency matrix.

## Calculating the PLR values ##

To calculate the PLR values we need the gold standard positive and negative associations. Execute to following code to do that:

```
#!terminal

python PLRCalcs.py plr
```

To do cross-validation:

```
#!terminal

python PLRCalcs.py crossvalidation
```

To do subsampling:

```
#!terminal

python PLRCalcs.py subsampling
```

To do bootstrapping:

```
#!terminal

python PLRCalcs.py boostrapping
```

## Creating adjacency matrix with PLR values ##

First we need to transform the raw target database files into evidence codes:

```
#!terminal

python ParseRaw_To_Evid.py
```

These files are going to be used to create the adjacency matrix with evidence codes. To get the adjacency matrix run the following code:

```
#!terminal

Rscript createTopol-0.9c.R [target_database_file] [mRNA_identifiers_adj_matrix] [miRNA_identifiers_adj_matrix] [identifiers_used_target_db]
```
Replace everything in between the "[]" with the appropriate file. For "[identifiers_used_target_db]" please indicate one of the following:  "refseq", "ensembl" or "entrez". Repeat the process for all target databases.

To combine the adjacency matrices in a single file, run the following:

```
#!terminal

python CombineEvids.py
```

The combined adjacency matrix with evidence codes can be transformed to a adjacency matrix running the following code:

```
#!terminal

python toPenalty.py
```

The adjacency matrix can be used in further analysis using the following script (where the PLR values are also expressed as penalties):

```
#!terminal

Rscript AnalysisAndFigs.R
```
However, I would recommend running this last script in an IDE, for example, Rstudio.

# Files and folders #

Here, I will explain something about the different files and folders that are present in this repository. The file or directory is mentioned first, the explanation follows.

```
#!terminal

/
```
The main folder that contains some scripts in Python and R.

```
#!terminal

/AnalysisAndFigs.R
```

The script that creates all the figures present in the publication and some extra stuff like:

* Clustering
* Correlation distributions
* Correlation distributions including simulations of randomly picking associations
* First and second order correlation calculations with filtering of target databases

```
#!terminal

/CombineEvids.py
```

Script that combines the evidence codes of multiples runs of a single target database.

```
#!terminal

/createTopol-0.9c.R
```

Script that creates and adjacency matrix filled with evidence codes.

```
#!terminal

/ParseRaw_To_Evid.py
```
The script that parses the raw target database files to evidence codes.

```
#!terminal

/PLRCalcs.py
```

This script can do the following:

* Calculate the PLR values
* Perform subsampling on PLR calculations
* Perform bootstrapping on PLR calculations
* Perform cross-validation on PLR calculations

```
#!terminal

/toPenalty.py
```

Script that reads the penalties and an adjacency matrix filled with evidence codes to create a adjacency matrix filled with PLR values. The PLR values are transformed to penalties in the "AnalysisAndFigs.R" script.

```
#!terminal

/AdjMat/
```

The folder that contains the adjacency matrices for different target databases. The filename indicates the target database that is used.

```
#!terminal

/Data/
```

The folder that contains the raw data from the STATegra project.

```
#!terminal

/Data/design.txt
```
Text file that explains the experimental design to EdgeR.

```
#!terminal

/Data/miRNA.txt
/Data/mRNA.txt
```

The log-values for miRNAs and mRNAs.

```
#!terminal

/Data/miRNADiff.txt
/Data/mRNADiff.txt
```

The identifiers of the differentially expressed miRNAs and mRNAs.

```
#!terminal

/Figs/
```

Output folder for the figures.


```
#!terminal

/GoldStandard/
```

Folder that contains the goldstandard positive (pos.txt) and negative (negs.txt) miRNA-mRNA associations.

```
#!terminal

/GSEA/
```

Folder that contains the GSEA results.

```
#!terminal

/GSEA/cluego/
```

Folder that contains the raw ClueGO results.


```
#!terminal

/GSEA/Top200/
```

Folder that contains the mRNA identifiers of the top 200 strongest correlated miRNA-mRNA pairs. Filtering was applied with a target database or penalized.

```
#!terminal

/GSEA/associatedBcellGenes.txt
```

File with identifiers of mRNAs that are associated with B-cells.

```
#!terminal

/GSEA/bcellTerms.txt
```

The results per set for B-cell terms.

```
#!terminal

/GSEA/GSEA.py
```

Script that can be run to parse the GSEA results.

```
#!terminal

/GSEA/GSEA.txt
```

The GSEA results for B-cell related terms.


```
#!terminal

/GSEA/penaltyMatrix.txt
```

An adjacency matrix filled with PLR values for the STATegra experiment.

```
#!terminal

/Subsampling/
```

Folder that contains the subsampling and cross-validation results.

```
#!terminal

/TargetDatabases/
```

The target databases, the raw files and the target databases with evidence codes.

# Who do I talk to? #
To me, obviously. Contact: Robbinbwm@gmail.com