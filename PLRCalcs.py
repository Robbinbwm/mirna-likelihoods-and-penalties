#Author:            Robbin Bouwmeester (supervisors: Johan Westerhuis and Frans van der Kloet)
#Purpose:           Calculating the PLR, doing resampling techniques and cross-validation
#Date:              18-09-2015
#Python version:    2.7.5
#Packages:          numpy V1.9
#Bugs:              No bugs found...

#Imports
import itertools
import random
from random import shuffle
import numpy
import sys

from scipy.integrate import simps
from numpy import trapz

#Instantiate vars
count_dict = {}

poss_evid = [0,1,2,3]
names_evid = {}
names_evid[0] = "targetscan"
names_evid[1] = "reptar"
names_evid[2] = "pictar"
names_evid[3] = "mirwalk"

evid_names = {}

evid_names["targetscan"] = 0
evid_names["reptar"] = 1
evid_names["pictar"] = 2
evid_names["mirwalk"] = 3

"""Function that calculates the frequency of specific positions in the bit vector evidence code
        Inputs:
            intfile - infile that contains two columns, the first column is the edge "mRNAIdentifier+miRNAIdentifier" and the second column "bit vector evidence codes"
			allowed - the positions that should be checked
        Outputs:
			counter - integer that counter the frequency of specific position
"""
def get_amount(intfile,allowed):
	#We start counting at 0...
	counter = 0
	for line in intfile:
		line = line.strip()
		line = line.split("\t")
		evids = line[1]
		edge = line[0]
		count_evid = True
		for evid_num in allowed:
			evid = evids[evid_num]
			if evid == "0":
				count_evid = False
		if count_evid: counter += 1
	return counter

"""Function that calculates the sensitivity for all possible positions in bit vector evidence code that can be turned on
	Inputs:
		infile - infile that contains two columns, the first column is the edge "mRNAIdentifier+miRNAIdentifier" and the second column "bit vector evidence codes"
				 INFILE IS THE POSITIVES OF GOLD STANDARD
	Outputs:
		sensitivity_dict - for possible combination the sensitivity
"""
def get_sensitivity(infile):
	sensitivity_dict = {}
	for i in range(1,len(poss_evid)+1):
		for comb in itertools.combinations(poss_evid,i):
			name = ""
			for ev in comb:
				name += names_evid[ev]+","
			name = name[:-1]
			count_dict[name] = get_amount(infile,comb)
	inv_map = {v: k for k, v in names_evid.items()}

	for key in count_dict.keys():
		evid_code = [0]*50
		evid_pos = ""
		for val in key.split(","):
			evid_pos += str(inv_map[val])+","
			evid_code[inv_map[val]] = 1
		sensitivity_dict[key] = count_dict[key]/float(len(infile))
	return sensitivity_dict

"""Function that calculates the specificity for all possible positions in bit vector evidence code that can be turned on
        Inputs:
            infile - infile that contains two columns, the first column is the edge "mRNAIdentifier+miRNAIdentifier" and the second column "bit vector evidence codes"
			INFILE IS THE NEGATIVES OF GOLD STANDARD
        Outputs:
            specificity_dict - for possible combination the specificity
"""
def get_num_sens(infile):
	sensitivity_dict = {}
	for i in range(1,len(poss_evid)+1):
		for comb in itertools.combinations(poss_evid,i):
			name = ""
			for ev in comb:
				name += names_evid[ev]+","
			name = name[:-1]
			count_dict[name] = get_amount(infile,comb)
	inv_map = {v: k for k, v in names_evid.items()}

	for key in count_dict.keys():
		evid_code = [0]*50
		evid_pos = ""
		for val in key.split(","):
			evid_pos += str(inv_map[val])+","
			evid_code[inv_map[val]] = 1
		sensitivity_dict[key] = count_dict[key]
	return sensitivity_dict

def get_num_spec(infile):
	specificity_dict = {}
	for i in range(1,len(poss_evid)+1):
		for comb in itertools.combinations(poss_evid,i):
			name = ""
			for ev in comb:
				name += names_evid[ev]+","
			name = name[:-1]
			count_dict[name] = get_amount(infile,comb)
	inv_map = {v: k for k, v in names_evid.items()}
	
	for key in count_dict.keys():
		evid_code = [0]*50
		evid_pos = ""
		for val in key.split(","):
			evid_pos += str(inv_map[val])+","
			evid_code[inv_map[val]] = 1
		specificity_dict[key] = count_dict[key]
	return specificity_dict

"""Function that calculates the specificity for all possible positions in bit vector evidence code that can be turned on
        Inputs:
            infile - infile that contains two columns, the first column is the edge "mRNAIdentifier+miRNAIdentifier" and the second column "bit vector evidence codes"
			INFILE IS THE NEGATIVES OF GOLD STANDARD
        Outputs:
            specificity_dict - for possible combination the specificity
"""	
def get_specificity(infile):
	specificity_dict = {}
	for i in range(1,len(poss_evid)+1):
		for comb in itertools.combinations(poss_evid,i):
			name = ""
			for ev in comb:
				name += names_evid[ev]+","
			name = name[:-1]
			count_dict[name] = get_amount(infile,comb)
	inv_map = {v: k for k, v in names_evid.items()}
	
	for key in count_dict.keys():
		evid_code = [0]*50
		evid_pos = ""
		for val in key.split(","):
			evid_pos += str(inv_map[val])+","
			evid_code[inv_map[val]] = 1
		specificity_dict[key] = 1.0-(count_dict[key]/float(len(infile)))
	return specificity_dict

"""Calculate PLR with subsampling
        Inputs:
            outfilePLR - specify output file PLR
			outfileSpec - specify output file specificity
			outfileSens - specify output file sensitivity
			goldPos - infile gold standard positives, two columns with edges and bit vector evidence code
			goldNeg - infile gold standard negatives, two columns with edges and bit vector evidence code
			verbose - vebosity?
			numpos - number of positives to select in case of subsampling. In case of bootstrapping change this number to max num positives. Important to note this must be a list, even if it is only 1 value.
			numneg - number of negatives to select in case of subsampling. In case of bootstrapping change this number to max num positives. Important to note this must be a list, even if it is only 1 value.
			num_reps - number of repeats subsampling
        Outputs:
            -
"""
def main(resampling=False,subsampling=True,verbose=True,num_iters=1000,
		outfilePLR="outPLR.txt",outfileSens="outSens.txt",outfileSpec="outSpec.txt",
		infile_neg="GoldStandard/negs.txt",infile_pos="GoldStandard/pos.txt"):
		
	neg_low = open(infile_neg).readlines()
	pos_low = open(infile_pos).readlines()
	
	if subsampling:
		if not resampling:
			print "If subsampling is specified must be able to resample"
	
	if resampling:
		if subsampling: 
			neg_steps = [18,35,54,70,74]
			pos_steps = [1215,2430,3644,4859,5871]
			replace_param = False
		else: 
			pos_steps = [5871]
			neg_steps = [74]
			replace_param = True

		for pos in pos_steps:
			for neg in neg_steps:
				plr_dict = {}
				sensitivity_dicts = {}
				specificity_dicts = {}
				for i in range(1,num_iters):
					if verbose:
						print "Iteration number:"
						print i
						print "Number of pos/neg:"
						print pos,neg
					strapped_samp_pos = numpy.random.choice(pos_low, size=pos,replace=replace_param)
					strapped_samp_neg = numpy.random.choice(neg_low, size=neg,replace=replace_param)
					sensitivity_dict = get_sensitivity(strapped_samp_pos)
					specificity_dict = get_specificity(strapped_samp_neg)
					
					for key in sensitivity_dict.keys():
						try:
							plr_val = sensitivity_dict[key]/(1-specificity_dict[key])
						except:
							plr_val = "NA"
						if plr_dict.has_key(key):
							plr_dict[key].append(plr_val)
							sensitivity_dicts[key].append(sensitivity_dict[key])
							specificity_dicts[key].append(specificity_dict[key])
						else:
							plr_dict[key] = []
							plr_dict[key].append(plr_val)
							
							sensitivity_dicts[key] = []
							sensitivity_dicts[key].append(sensitivity_dict[key])
							
							specificity_dicts[key] = []
							specificity_dicts[key].append(specificity_dict[key])

				outfile = open(outfilePLR,"a")
				for key in plr_dict.keys():
					outfile.write("%s\t%s\t%s\t%s\n" % (key,pos,neg,"\t".join(map(str, plr_dict[key]))))
				outfile.close()
				
				outfile = open(outfileSens,"a")
				for key in plr_dict.keys():
					try:
						outfile.write("%s\t%s\t%s\t%s\n" % (key,pos,neg,"\t".join(map(str, sensitivity_dicts[key]))))
					except:
						print key,sensitivity_dicts[key]
				outfile.close()
				
				outfile = open(outfileSpec,"a")
				for key in plr_dict.keys():
					outfile.write("%s\t%s\t%s\t%s\n" % (key,pos,neg,"\t".join(map(str, specificity_dicts[key]))))
				outfile.close()
	else:
		sensitivity_dicts = {}
		specificity_dicts = {}
		
		plr_dict = {}
		sensitivity_dict = get_sensitivity(pos_low)
		specificity_dict = get_specificity(neg_low)
		sensitivity_num = get_num_sens(pos_low)
		specificity_num = get_num_spec(neg_low)
		
	
		for key in sensitivity_dict.keys():
			try:
				plr_val = sensitivity_dict[key]/(1-specificity_dict[key])
			except:
				plr_val = "NA"
			if plr_dict.has_key(key):
				plr_dict[key].append(plr_val)
				sensitivity_dicts[key].append(sensitivity_dict[key])
				specificity_dicts[key].append(specificity_dict[key])
			else:
				plr_dict[key] = []
				plr_dict[key].append(plr_val)
				
				sensitivity_dicts[key] = []
				sensitivity_dicts[key].append(sensitivity_dict[key])
				
				specificity_dicts[key] = []
				specificity_dicts[key].append(specificity_dict[key])

		outfile = open(outfilePLR,"w")
		for key in plr_dict.keys():
			outfile.write("%s\t%s\t%s\t%s\n" % (key,"\t".join(map(str, plr_dict[key])),sensitivity_num[key],specificity_num[key]))
		outfile.close()
		
		outfile = open(outfileSens,"w")
		for key in plr_dict.keys():
			try:
				outfile.write("%s\t%s\n" % (key,"\t".join(map(str, sensitivity_dicts[key]))))
			except:
				print key,sensitivity_dicts[key]
		outfile.close()
		
		outfile = open(outfileSpec,"w")
		for key in plr_dict.keys():
			outfile.write("%s\t%s\n" % (key,"\t".join(map(str, specificity_dicts[key]))))
		outfile.close()

"""Function that creates edges from dictionary
        Inputs:
            dict - dictionary of any sort
			identifiers - identifiers that should be extracted from dict
        Outputs:
            return_l - list with values that are lines or edges
"""
def get_edges(dict,identifiers):
	return_l = []
	for id in identifiers:
		return_l.append("%s\t%s\n" % (id,dict[id]))
	return return_l

"""Function that assigns a PLR value to a specific evidence code
        Inputs:
            dict - list that contains an edge (consisting of miRNA and mRNA) and a evidence code split with a "\t"
			plr_dict - dictionary that translates a evidence code to a plr value
			pos_neg_dict - dictionary with an edge (consisting of miRNA and mRNA) and if the gold standard says it is positive or negative
			evid_nums - evidence code combinations to check
        Outputs:
            pos_ret,neg_ret - the maximum PLR that should be assigned to a specific edge (consisting of miRNA and mRNA)
"""
def assign_plr(dict,plr_dict,evid_nums = [0,1,2,3]):
	return_l = []
	for l in dict:
		id = l.split("\t")[0]
		evid = l.split("\t")[1].rstrip()
		evid_on = []
		for pos in evid_nums: 
			if evid[pos] != "0": evid_on.append(pos)
		if len(evid_on) == 0:
			return_l.append([id,0])
			continue
		
		combs = []
		for i in xrange(1, len(evid_on)+1):
			els = [list(x) for x in itertools.combinations(evid_on, i)]
			combs.extend(els)
			
		plr = []	
		for c in combs:
			c.sort()
			plr.append(plr_dict[",".join(map(str,c))])

		return_l.append([id,max(plr)])

	return return_l	

"""Function that calculates the coordinations of the ROC-plot
        Inputs:
			ordered - two dimensional list that contains edges (consisting of miRNA and mRNA) and a sorting value
        Outputs:
            false_positive_rate, true_positive_rate - list of fpr and tpr coords
"""
def roc_plot(ordered):
	false_negatives = 0
	true_negatives = 0
	true_positives = 0
	false_positives = 0
	##ignore entries in the  benchmarkDB dict classified as "ambiguous"
	for id in ordered:
		if id[1] == "negative":
			false_negatives += 1
		if id[1] == "positive":
			true_negatives += 1

	true_positive_rate = [0]
	false_positive_rate = [0]
	prev_score = -1
	first = True
	for item in ordered:
		## We have not counted TP, FP, TN and FN yet, so at first iteration, don't update TPR and FPR
		if prev_score != item[0] and not first: #
			aux_pos = float(true_positives + false_negatives)
			aux_neg = float(false_positives + true_negatives)
			if aux_pos != 0:
				true_positive_rate.append(true_positives / aux_pos)
			if aux_neg != 0:
				false_positive_rate.append(false_positives / aux_neg)
		first = False
		try:
			if item[1] == "negative":
				true_positives += 1
				false_negatives -= 1
			if item[1] == "positive":
				false_positives += 1
				true_negatives -= 1
		except KeyError:
			print "Could not find the entry in the benchmark file, please make sure all the blast \"pairs\" are also in the benchnmark file."
			
		prev_score = item[0]
	#append the very last item to the list, since the last item will be the same as the previous item.
	aux_pos = float(true_positives+false_negatives)
	aux_neg = float(false_positives + true_negatives)
	if aux_pos != 0:
		true_positive_rate.append(true_positives/aux_pos)
	if aux_neg != 0:
		false_positive_rate.append(false_positives/aux_neg)
	return false_positive_rate, true_positive_rate
	
"""Function that does a naive integration of coordinates
        Inputs:
            x - the x coordinates
			y - the y coordinates
        Outputs:
			totalSurface - the integration value
"""
def integrate(x,y):
	#Uncomment below to use either the numpy or scipy package to calculate the AUC
	#return trapz(y,x)
	#return simps(y,x)
	
	#Start at 0 surface
    totalSurface = 0
	
	#How much iterations do we need?
    iterLength = len(x)-1
	#Iterate over x,y points
    for counter in range(iterLength):
		#Calculate surface by assuming rectangles, take the previous middle value and the next to calculate the surface between points, sum it
        totalSurface += (0.63*(y[counter+1]+y[counter]))*(x[counter+1]-x[counter])
    return totalSurface

"""Function that does a cross-validation on the PLR calculation
        Inputs:
            see "help" parameter in parse_commandline()"
        Outputs:
            stdout - exit status of process
"""
def crossvalid(randomize=False,goldneg="GoldStandard/negs.txt",
			   goldpos="GoldStandard/pos.txt",
			   num_repeats=100,outfile="auc.txt",nfolds=10,verbose=True):
	#Read gold standard
	neg_low = open(goldneg).readlines()
	pos_low = open(goldpos).readlines()
	
	#Some other vars that need to be initiated...
	neg_pairs = []
	pos_pairs = []
	
	neg_evid = {}
	pos_evid = {}
	neg_or_pos = {}
	
	#Get the pairs in previous defined vars, specific for positive or negatives
	if not(randomize):
		for pair in pos_low:
			pos_pairs.append(pair.split("\t")[0])
			pos_evid[pair.split("\t")[0]] = pair.split("\t")[1]
			neg_or_pos[pair.split("\t")[0]] = "positive"
	
		for pair in neg_low:
			neg_pairs.append(pair.split("\t")[0])
			neg_evid[pair.split("\t")[0]] = pair.split("\t")[1]
			neg_or_pos[pair.split("\t")[0]] = "negative"
	
	#Check if there is no overlap between positive and negative
	for x in list(set(neg_pairs).intersection(set(pos_pairs))):
		pos_pairs.remove(x)
	
	total_int = []
	total_int2 = []
	
	if verbose: print "Going to perform cross-validation..."
	
	#Repeat the cross-validation X times
	for j in range(num_repeats):
		if verbose: print "Repeat number: %s" % (j+1)
		
		total_neg = []
		#Divide pairs in 10 sets, for the negatives
		for training, validation in k_fold_cross_validation(neg_pairs, nfolds,randomize=True):
			for item in neg_pairs:
				try: assert (item in training) ^ (item in validation)
				except: pass
			total_neg.append([training,validation])		
		total_pos = []
		
		#Divide pairs in 10 sets, for the positives
		for trainingPos, validationPos in k_fold_cross_validation(pos_pairs, nfolds,randomize=True):
			for item1 in pos_pairs:
				try: assert (item1 in trainingPos) ^ (item1 in validationPos)
				except: pass
			total_pos.append([trainingPos,validationPos])
		plr_valid = []
		
		#Iterate over all 10 sets
		for i in range(len(total_pos)):
			#Get the train/test sets
			train = total_neg[i][0]
			validation = total_neg[i][1]
			
			train_pos = total_pos[i][0]
			validation_pos = total_pos[i][1]
			
			#Get the edges for the sets
			train_evid = get_edges(neg_evid,train)
			validation_evid = get_edges(neg_evid,validation)
			
			train_pos_evid = get_edges(pos_evid,train_pos)
			validation_pos_evid = get_edges(pos_evid,validation_pos)
			
			#Calculate sensitivity/specificity for training sets
			sensitivity_dict = get_sensitivity(train_pos_evid)
			specificity_dict = get_specificity(train_evid)
			
			#Calculate the PLR values
			plr_dict = {}
			for key in sensitivity_dict.keys():
				try:
					plr_val = sensitivity_dict[key]/(1-specificity_dict[key])
				except:	
					plr_val = 5

				bit_name = []
				for k in key.split(","): bit_name.append(int(evid_names[k]))
				bit_name.sort()
				bit = ",".join(map(str,bit_name))
				#Need to multiply by large factor because ordering on floating decimals does not work properly
				plr_dict[bit] = plr_val*1000000
			
			#Get the maximum PLR value that is available for the validation set
			validation_evid.extend(validation_pos_evid)
			plr_valid.extend(assign_plr(validation_evid,plr_dict))

		#Sort pairs on PLR value	
		plr_valid.sort(key=lambda x: int(x[1]),reverse=True)
		
		temp_dict = {}
		
		#Store edges with assigned PLR value
		roc_list = []
		for id in plr_valid:
			id_new = id[0]
			roc_list.append([id[1],neg_or_pos[id_new]])
			if temp_dict.has_key(id[1]):temp_dict[id[1]].append(neg_or_pos[id_new])
			else: temp_dict[id[1]] = [neg_or_pos[id_new]]
		plrs = temp_dict.keys()
		plrs.sort(reverse=True)
		roc_list2 = []
		counter = 0
		for plr in plrs:
			temp_dict[plr].sort(reverse=True)
			for i in temp_dict[plr]:
				counter += 1
				roc_list2.append([counter,i])
		#Get the coords of the ROC-plot
		coords = roc_plot(roc_list)
		total_int.append(integrate(coords[1],coords[0]))
		print integrate(coords[1],coords[0])
		# Make sure we can get the random AUC!!!
		#random.shuffle(roc_list)
		#coords_r = roc_plot(roc_list)
		#total_int2.append(integrate(coords_r[1],coords_r[0]))
	if verbose: print "Done with cross-validation..."
	if verbose: print "Writing results to file: %s" % (outfile)
	
	#Write the AUC values to a file	
	outfile = open(outfile,"w")
	outfile.write("\n".join(map(str,total_int)))
	outfile.close()
	#outfile2 = open("auc_random.txt","w")
	#outfile2.write("\n".join(map(str,total_int2)))
	#outfile2.close()

"""Function that writes coordinates of for example a ROC-plot to a file 
        Inputs:
            coords - the coordinates
			unique_identifier - write unique identifier to outfile
			outfile_name - name of the outfile
        Outputs:
            -
"""	
def write_coords(coords,unique_identifier="",outfile_name = "outROC.txt"):
	outfile = open(outfile_name,"a")
	for coord in range(len(coords[1])):
		outfile.write("%s\t%s\t%s\n" % (unique_identifier,coords[1][coord],coords[0][coord]))
	outfile.close()

"""Function that returns multiple sets of original set, used for cross-validation
        Inputs:
            items - the pairs to divide into sets
			k - number of sets
        Outputs:
            training, validation - return training/validation sets
"""
def k_fold_cross_validation(items, k, randomize=False):
	#should we randomize the sets? Otherwise order is important
	if randomize:
		items = list(items)
		shuffle(items)
	
	#Make slices of pairs in k slices
	slices = [items[i::k] for i in xrange(k)]
	
	#Iterate over sets, assign specific training/validation sets
	for i in xrange(k):
		validation = slices[i]
		training = [item
					for s in slices if s is not validation
					for item in s]
		yield training, validation

if __name__ == '__main__':
	#Perform PLR calculations, bootstrapping, subsampling and cross-validation
	try:
		p = sys.argv[1]
	except:
		print "Please specify something to do. One of the following: 'subsampling', 'crossvalidation', 'plr' or 'bootstrapping'"
		sys.exit(-1)
	if p == "subsampling": main(resampling=True,subsampling=True,outfilePLR="subsampPLR.txt",outfileSens="subsampSens.txt",outfileSpec="subsampSpec.txt")
	elif p == "bootstrapping": main(resampling=True,subsampling=False,outfilePLR="bootstrapPLR.txt",outfileSens="bootstrapSens.txt",outfileSpec="bootstrapSpec.txt")
	elif p == "plr": main(resampling=False,outfilePLR="outPLR.txt",outfileSens="outSens.txt",outfileSpec="outSpec.txt")
	elif p == "crossvalidation": crossvalid()
	else:
		print "Please specify something to do. One of the following: 'subsampling', 'crossvalidation', 'plr' or 'bootstrapping'"
		sys.exit(-1)