#Author:            Robbin Bouwmeester (supervisors: Johan Westerhuis and Frans van der Kloet)
#Purpose:           Combining the evidence codes of multiple files
#Date:              18-09-2015
#Python version:    2.7.5
#Bugs:              No bugs found...

import itertools


"""Function that determines the maximum PLR for an evidence code
		Inputs:
			evid_code - the evidence code
			evid_dict - the dictionary with evidence codes and PLR values
		Outputs:
			 - the maximum PLR associated with and evidence code
"""	
def get_likelihood(evid_code,evid_dict,ignore_pos=[1,3,7,8,9,10,11,12,13,14,15,16,32,33,34,35,36,37],max_penalty=0.0):
	new_evid_code = ""
	
	#If there are all "0" it must get the max penalty
	if evid_code.count("0") == 50:
		return max_penalty
	
	#Iterate over positions in evidence code and ignore some of the positions
	for pos in range(0,len(evid_code)):
		evi = evid_code[pos]
		evi = int(evi)
		if evi > 1:
			evi = 1
		if pos in ignore_pos: new_evid_code += "0"
		else: new_evid_code += str(evi)
	
	#What positions are turned on?
	posses = findOccurences(new_evid_code,"1")
	if new_evid_code.count("0") == 50:
		return max_penalty
	
	#Get the possible combinations that can match
	combs = []
	for i in xrange(1, len(posses)+1):
		els = [list(x) for x in itertools.combinations(posses, i)]
		combs.extend(els)
	
	#Get the PLR of possible combinations
	plr = []	
	for c in combs:
		c.sort()
		plr.append(evid_dict[",".join(map(str,c))])
	
	#Return the maximum PLR
	return max(plr)

"""Function counts the occurrence of a character
		Inputs:
			s - string to count occurrence
			ch - the character to match
		Outputs:
			 - the occurrences of character in vector
"""	
def findOccurences(s, ch):
	#Get the number of a certain character
    return [i for i, letter in enumerate(s) if letter == ch]

"""Function that reads the PLR associated with evidence codes and returns a dictionary
	Inputs:
		evid_file - tab separated file with evidence code and their PLR
	Outputs:
		evid_dict - dictionary that joins the evidence codes with their respective PLR
"""	
def get_evid_dict(evid_file):
	#Read the evidence file
	evid_file = open(evid_file)

	#What evidence is present on what position?
	names_evid = {}
	names_evid[1] = "mirtarbase"
	names_evid[2] = "targetscan"
	names_evid[3] = "mirecords"
	names_evid[4] = "reptar"
	names_evid[5] = "pictar"
	names_evid[6] = "mirwalk"
	names_evid[7] = "connectiontable"
	names_evid[32] = "targetscanLow"
	names_evid[33] = "targetscanMedium"
	names_evid[34] = "targetscanHigh"
	names_evid[35] = "reptarLow"
	names_evid[36] = "reptarMedium"
	names_evid[37] = "reptarHigh"
	#Make an inverse mapping of dictionary (turn around the keys and values)
	inv_map = {v: k for k, v in names_evid.items()}
	
	evid_dict = {}
	
	for line in evid_file:
		line = line.strip()
		orig_evid = line.split("\t")[0].split(",")
		evid_code = [0]*50
		nevid_code = []
		
		#Determine what evidence is present
		for val in orig_evid:
			evid_code[inv_map[val]] = 1
			nevid_code.append(inv_map[val])
		
		#Get the PLR value associated with evidence code
		nevid_code.sort()
		evid_dict[",".join(map(str,nevid_code))] = line.split("\t")[1]
	return evid_dict

"""Wrapper to go from an adjacency matrix with evidence codes to a PLR filled matrix
		Inputs:
			matrix - two-dimensional list with evidence codes
		Outputs:
			 -
"""	
def translate_evid_penalty(matrix,evid_dict,outfile="penaltyMatrix.txt"):
	outfile = open(outfile,"w")
	
	#Iterate over columns and rows
	for row in matrix:
		for column in row:
			#Get the likelihood and write to file
			outfile.write(str(get_likelihood(column,evid_dict)))
			outfile.write("\t")
		#Start a new row
		outfile.write("\n")
	outfile.close()
	
"""Function that reads a matrix and puts it into a two dimensional list
	Inputs:
		infile - tab and hard return seperated matrix
	Outputs:
		 - two-dimensional list of matrix
"""	
def read_matrix(infile,verbose=False):
	global nrow
	global ncol
	
	if verbose: print infile
	
	#Make an initial matrix with "0" values
	matrix = [["0" for i in range(nrow)] for j in range(ncol)]
	
	#Read the matrix
	infile = open(infile)
	infile = infile.readlines()

	#Iterate over rows and columns
	for row in range(1,len(matrix)+1):
		row_line = infile[row].strip().split("\t")
		for column in range(1,len(matrix[0])+1):
			try:
				#Set the values in initial matrix
				val = row_line[column].split("-")[0]
				matrix[row-1][column-1] = val
			except:
				print matrix[row]
				print len(matrix[row])
				print column
	return matrix

	
"""Function that returns the dimensions of a two-dimensional matrix that is tab and hard return separated
	Inputs:
		infile - tab and hard return separated matrix
	Outputs:
		 - the number of columns and the number of rows
"""	
def get_dim_matrix(infile,verbose=False):
	if verbose: print infile
	infile = open(infile)
	infile = infile.readlines()
	
	#Determine dimensions and return
	return(len(infile),len(infile[0].split("\t")))

#Get the number of columns and rows	
ncol,nrow = get_dim_matrix("combined.txt")

#Write the PLR values to dictionary
evid_dict = get_evid_dict("penaltiesOut.txt")

#Read adjacency matrix with bit vectors
matrix = read_matrix("combined.txt")

#Express evidence codes as PLR
translate_evid_penalty(matrix,evid_dict)